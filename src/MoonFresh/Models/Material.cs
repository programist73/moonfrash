﻿using MongoDB.Bson;
using System;
using DbsNetCore.Interfaces;
using DbsNetCore.Models;

namespace MoonFresh.Models
{
    public class Material:BaseModel,IModelWithImage,IModel
    {

        /// <summary>
        /// Транслит заголовка
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// Подзаголовок
        /// </summary>
        public string Subtitle { get; set; }
        /// <summary>
        /// Описание
        /// </summary>                                    
        public string Description { get; set; }
        /// <summary>
        /// Текст включая Html теги
        /// </summary>
        public string Text { get; set; }
        /// <summary>
        /// Автор
        /// </summary>
        public string Author { get; set; }
        /// <summary>
        /// Дата публикации
        /// </summary>
        public DateTime Date { get; set; }
        /// <summary>
        /// Путь к изображению
        /// </summary>
        public string PathToImage { get; set; }
        /// <summary>
        /// Id категории
        /// </summary>
        public ObjectId CategoryId { get; set; }

        public int Rate { get; set; }
        public string Tags { get; set; }

    }
}
