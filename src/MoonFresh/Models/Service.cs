﻿using DbsNetCore.Interfaces;
using DbsNetCore.Models;
using MongoDB.Bson;

namespace MoonFresh.Models
{
    public class Service : BaseModel, IModel, IModelWithImage
    {
        public ObjectId CategoryId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string More { get; set; }
        public int Price { get; set; }
        public string PathToImage { get; set; }
    }
}
