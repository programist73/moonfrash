﻿using DbsNetCore.Interfaces;
using DbsNetCore.Models;

namespace MoonFresh.Models
{
    public class GalleryCategory :BaseModel,IModel,IModelWithImage
    {
        public string Title { get; set; }
        public string PathToImage { get; set; }

       
    }
}
