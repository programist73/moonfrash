﻿using DbsNetCore.Interfaces;
using DbsNetCore.Models;

namespace MoonFresh.Models
{
    public class Property :PropertyBase,IModel
    {
       
        public About About {get;set;}
        public string Addres { get; set; }
        public double Long { get; set; }
        public double Lat { get; set; }
        public string Email { get; set; }

        public Property():base()
        {

            About = new About();
           
        }
    }
}
