﻿using DbsNetCore.Interfaces;
using DbsNetCore.Models;
using MongoDB.Bson;

namespace MoonFresh.Models
{
    public class Gallery :BaseModel,IModel,IModelWithImage
    {
        public ObjectId CategoryId { get; set; }
        public string PathToImage { get; set; }
        public string Alt { get; set; }
    }
}
