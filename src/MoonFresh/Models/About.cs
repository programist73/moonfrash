﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoonFresh.Models
{
    public class About
    {
        public string Title{get;set;}
        public string Description { get; set; }
        public string More { get; set; }
        public string PathToImage { get; set; }
    }
}
