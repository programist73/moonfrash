﻿using DbsNetCore.Interfaces;
using DbsNetCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoonFresh.Models
{
    public class ServiceCategory : BaseModel, IModel, IModelWithImage,IModelTitle
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string PathToImage { get; set; }

    }
}
