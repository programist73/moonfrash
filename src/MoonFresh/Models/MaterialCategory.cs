﻿using MongoDB.Bson;
using DbsNetCore.Interfaces;
using DbsNetCore.Models;

namespace MoonFresh.Models
{
    public class MaterialCategory:BaseModel,IModel,IModelTitle
    {

        /// <summary>
        /// Заголовок в транслите 
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// Описание
        /// </summary>
        public string Text { get; set; }
        /// <summary>
        /// Id родительской категории
        /// </summary>
        public ObjectId ParrentCategoryId { get; set; }

    }
}
