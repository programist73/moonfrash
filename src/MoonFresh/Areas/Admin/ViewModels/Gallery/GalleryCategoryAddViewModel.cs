﻿using DbsNetCore.Inrefaces;
using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace MoonFresh.Areas.Admin.ViewModels.Gallery
{
  public class GalleryCategoryAddViewModel :IViewModel,IViewModelImage
    {
        [Required]
        [Display(Name="Название категории")]
        public string Title { get; set; }
        
        [Required]
        [Display(Name ="Изображение")]
        public IFormFile Image { get; set; }
        public string PathToImage { get; set; }

        [Display(Name ="Вкл./Откл.")]
        public bool IsActive { get; set; }
    }
}