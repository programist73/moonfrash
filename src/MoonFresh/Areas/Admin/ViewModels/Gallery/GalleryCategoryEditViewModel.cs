﻿using DbsNetCore.Inrefaces;
using Microsoft.AspNetCore.Http;
using MongoDB.Bson;
using MoonFresh.Areas.Admin.ViewModels;
using System.ComponentModel.DataAnnotations;

namespace MoonFresh.Areas.Admin.ViewModels.Gallery
{
    public class GalleryCategoryEditViewModel:GalleryCategoryAddViewModel,IViewModelImage,IViewModel
    {
        [Display(Name ="Старое изображение")]
        new public string PathToImage { get; set; }
        [Display(Name = "Изображение")]
        new public IFormFile Image { get; set; }
        public string  Id { get; set; }

    }
}