﻿
using DbsNetCore.Inrefaces;
using Microsoft.AspNetCore.Http;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MoonFresh.Areas.Admin.ViewModels.Gallery
{
    public class GalleryEditViewModel: GalleryAddViewModel,IViewModelImage, IViewModel
    {
        [Display(Name = "Старое изображение")]
        new public string PathToImage { get; set; }
        [Display(Name = "Изображение")]
        new public IFormFile Image { get; set; }

        public string Id { get; set; }
    }
}
