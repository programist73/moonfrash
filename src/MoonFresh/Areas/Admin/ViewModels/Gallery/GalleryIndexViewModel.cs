﻿using DbsNetCore.Utils.Helpers.Models;
using System.Collections.Generic;

namespace MoonFresh.Areas.Admin.ViewModels.Gallery
{
    public class GalleryIndexViewModel
    {
       public  PageInfo PageInfo { get; set; }
        public IList<Models.Gallery> Galleres { get; set; }
        public IList<Models.GalleryCategory> GalleryCategories { get; set; }

    }
}
