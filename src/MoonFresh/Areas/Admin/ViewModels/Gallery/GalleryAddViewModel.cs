﻿using DbsNetCore.Inrefaces;
using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace MoonFresh.Areas.Admin.ViewModels.Gallery
{
    public class GalleryAddViewModel:IViewModelImage,IViewModel
    {
        [Display(Name="Категория галлереи")]
        public string CategoryId { get; set; }
        [Display(Name = "Изображение")]
        public string PathToImage { get; set; }
        [Display(Name = "Изображение")]
        public IFormFile Image { get; set; }
        [Display(Name = "Описание")]
        public string Alt { get; set; }
        [Display(Name = "Опубликовано")]
        public bool IsActive { get; set; }
    }
}