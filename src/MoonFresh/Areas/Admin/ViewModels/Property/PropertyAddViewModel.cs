﻿using MoonFresh.Models;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using DbsNetCore.Inrefaces;
using DbsNetCore.Models;

namespace MoonFresh.Areas.Admin.ViewModels.Property
{
    public class PropertyAddViewModel:IViewModel
    {

        [Display(Name = "Наименование конфигурации")]
        public string Name { get; set; }
          
        [Required, Display(Name = "Режим работы")]
        public string TimeOfWorck { get; set; }
        [Required, Display(Name = "Телефон")]
        public IDictionary<string, string> Phone { get; set; }
       
        public string PathToLogo { get; set; }
        [Display(Name = "Логотип"),Required]
        public IFormFile Logo { get; set; }
        [Display(Name = "Изображение"), Required]
        public IFormFile Image { get; set; }
        [Display(Name = "Яндекс метрика")]
        public string Yascript { get; set; }
        [Display(Name = "Гугл аналитика")]
        public string Gscript { get; set; }
        public string KeyWords { get; set; }
        [Required, Display(Name = "Описание")]
        public string Description { get; set; }
        public MailSetting MailSetting { get; set; }
        public IList<Social> Social { get; set; }
        public About About { get; set; }
        public string Addres { get; set; }
        public string Email { get; set; }

    }
}
