﻿using MoonFresh.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using DbsNetCore.Inrefaces;
using DbsNetCore.Models;

namespace MoonFresh.Areas.Admin.ViewModels.Property
{
    public class PropertyEditViewModel:PropertyAddViewModel,IViewModel
    {
        public string Id { get; set; }
        public List<Social> NewSocial { get; set; }
        public IDictionary<string, string> NewPhone { get; set; }
        [Display(Name="Старый логотоп")]
        new public string PathToLogo { get; set; }
        [Display(Name="Логотип")]
        new public  IFormFile Logo { get; set; }
        [Display(Name = "Изображение")]
        new public IFormFile Image { get; set; }
        //public PropertyEditViewModel()
        //{
        //    NewSocial = new List<MoonFresh.Models.Social>();
        //    NewPhone = new Dictionary<string, string>();
        //}
    }   
}
