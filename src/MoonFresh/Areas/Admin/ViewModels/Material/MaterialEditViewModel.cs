﻿using DbsNetCore.Inrefaces;
using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace MoonFresh.Areas.Admin.ViewModels.Material
{
    public class MaterialEditViewModel:MaterialAddViewModel,IViewModel,IViewModelImage
    {
        public string Id { get; set; }

        [Display(Name="Старое изображение")]
        new public string PathToImage { get; set; }
        [Display(Name = "Изображение")]
       new  public IFormFile Image { get; set; }
    }
}
