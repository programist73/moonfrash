﻿using DbsNetCore.Inrefaces;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoonFresh.Areas.Admin.ViewModels.Material
{
    public class MaterialCategoryEditViewModel:MaterialCategoryAddViewModel,IViewModel
    {
           public string Id { get; set; }
    }
}
