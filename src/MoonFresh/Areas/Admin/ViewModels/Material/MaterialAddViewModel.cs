﻿using DbsNetCore.Inrefaces;
using Microsoft.AspNetCore.Http;
using System;
using System.ComponentModel.DataAnnotations;

namespace MoonFresh.Areas.Admin.ViewModels.Material
{
    public class MaterialAddViewModel:IViewModel,IViewModelImage
    {
        /// <summary>
        /// Транслит заголовка
        /// </summary>
        [Display(Name ="Заголовок")]
        public string Title { get; set; }
        /// <summary>
        /// Подзаголовок
        /// </summary>
        [Display(Name = "Подзаголовок")]
        public string Subtitle { get; set; }
        /// <summary>
        /// Описание
        /// </summary>   
        [Display(Name = "Описание")]
        public string Description { get; set; }
        /// <summary>
        /// Текст включая Html теги
        /// </summary>
        [Display(Name = "Текст")]
        public string Text { get; set; }
        /// <summary>
        /// Автор
        /// </summary>
        [Display(Name = "Автор")]
        public string Author { get; set; }
        /// <summary>
        /// Дата публикации
        /// </summary>
        [Display(Name = "Дата публикации")]
        [DataType(DataType.Date)]
        public DateTime Date { get; set; }
        /// <summary>
        /// Путь к изображению
        /// </summary>
        [Display(Name = "Изображение")]
        public string PathToImage { get; set; }
        /// <summary>
        /// Id категории
        /// </summary>
        [Display(Name = "Категория статьи")]
        public string CategoryId { get; set; }

        [Display(Name = "Опубликовано")]
        public bool IsActive { get; set; }


        [Required]
        [Display(Name = "Изображение")]
        public IFormFile Image { get; set; }

        [Display(Name = "Количество просмотров")]
        public int Rate { get; set; }
        [Display(Name = "Хэш теги")]
        public string Tags { get; set; }
    }
}
