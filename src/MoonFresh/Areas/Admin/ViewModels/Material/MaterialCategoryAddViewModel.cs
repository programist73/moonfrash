﻿using DbsNetCore.Inrefaces;
using System.ComponentModel.DataAnnotations;

namespace MoonFresh.Areas.Admin.ViewModels.Material
{
    public class MaterialCategoryAddViewModel:IViewModel
    {

        [Display(Name = "Опубликовано")]
        public bool IsActive { get; set; }

        /// <summary>
        /// Заголовок в транслите 
        /// </summary>
        [Display(Name = "Заголовок")]
        public string Title { get; set; }
        /// <summary>
        /// Описание
        /// </summary>
        [Display(Name = "Описание")]
        public string Text { get; set; }

    }
}
