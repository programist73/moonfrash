﻿using MoonFresh.Models;
using System.Collections.Generic;
using DbsNetCore.Utils.Helpers.Models;

namespace MoonFresh.Areas.Admin.ViewModels.Material
{
    public class MaterialIndexViewModel
    {
        public PageInfo PageInfo { get; set; }
        public List<MaterialCategory> MaterialCategoryes { get; set; }
        public List<Models.Material> Materials { get; set; }
    }
}
