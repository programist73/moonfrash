﻿using DbsNetCore.Inrefaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace MoonFresh.Areas.Admin.ViewModels.Service
{
    public class ServiceCategoryAddViewModel : IViewModel, IViewModelImage
    {
        [Display(Name="Заголовок категории"),Required]
        public string Title { get; set; }
        [Display(Name = "Описание"), Required]
        public string Description { get; set; }
        [Display(Name = "Иконка"), Required]
        public IFormFile Image { get; set; }
        public string PathToImage { get; set; }
        public bool IsActive { get; set; }
    }
}
