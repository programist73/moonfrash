﻿using DbsNetCore.Inrefaces;
using Microsoft.AspNetCore.Http;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MoonFresh.Areas.Admin.ViewModels.Service
{
    public class ServiceEditViewModel:ServiceAddViewModel, IViewModel, IViewModelImage
    {
        public string Id { get; set; }
        [Display(Name="Старая иконка")]
        public new string PathToImage { get; set; }
        [Display(Name = "Иконка")]
        public new IFormFile Image { get; set; }
    }
}
