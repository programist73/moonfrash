﻿using Microsoft.AspNetCore.Http;
using DbsNetCore.Inrefaces;
using System.ComponentModel.DataAnnotations;

namespace MoonFresh.Areas.Admin.ViewModels.Service
{
    public class ServiceAddViewModel:IViewModel,IViewModelImage
    {
        [Display(Name = "Заголовок"),Required]
        public string Title { get; set; }
        [Display(Name = "Описание"),Required]
        public string Description { get; set; }
        [Display(Name = "Подробнее"),Required]
        public string More { get; set; }
        [Display(Name = "Изображение")]
        public string PathToImage { get; set; }
        [Display(Name = "Иконка"),Required]
        public IFormFile Image { get; set; }
        [Display(Name = "Стоимость"),Required]
        public int Price { get; set; }
        [Display(Name = "Категория услуги")]
        public string CategoryId { get; set; }
        [Display(Name = "Опубликовано")]
        public bool IsActive { get; set; }

    }
}
