﻿using DbsNetCore.Inrefaces;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MoonFresh.Areas.Admin.ViewModels.Service
{
    public class ServiceCategoryEditViewModel:ServiceCategoryAddViewModel,IViewModel,IViewModelImage
    {
        public string Id { get; set; }
        [Display(Name = "Старая иконка")]
        public new string PathToImage { get; set; }
        [Display(Name = "Иконка")]
        public new IFormFile Image { get; set; }
    }
}
