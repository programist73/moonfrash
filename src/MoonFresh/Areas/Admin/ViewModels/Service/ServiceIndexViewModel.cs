﻿using MoonFresh.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoonFresh.Areas.Admin.ViewModels.Service
{
    public class ServiceIndexViewModel
    {
        public IList<ServiceCategory> Categoryes { get; set; }
        public IList<Models.Service> Services { get; set; }

    }
}
