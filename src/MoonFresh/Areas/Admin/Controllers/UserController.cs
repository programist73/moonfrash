﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using MongoDB.Bson;
using DbsNetCore.Models.Identity.ViewModels.User;
using DbsNetCore.Models.Identity;
using DbsNetCore.Utils;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace MoonFresh.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize]
    public class UserController : Controller
    {
        private readonly IRepository _repository;

        public UserController(IRepository repository)
        {
            _repository = repository;
        }

        // GET: /<controller>/
        public async Task<IActionResult> Index()
        {
            var users =await _repository.Users.Get();


            return View(users);
        } 

        [HttpGet]
        public IActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Add(UserAddViewModel uavm)
        {
             if(ModelState.IsValid)
            {
                var user =new  User();
                user = AutoMapper<UserAddViewModel, User>.Mapping(uavm, user);
                string pwd =Utils.CalculateMD5Hash(user.Password);
                user.Password = pwd;
                await _repository.Users.Save(user);
                return RedirectToAction("Index");


            }
             else
            {
                return View(uavm);
            }
        }

        [HttpGet]
        public async Task<IActionResult> Edit(string id)
        {
            if (string.IsNullOrEmpty(id))
                return StatusCode(400);
            var user = await _repository.Users.GetOne(x => x.Id == new ObjectId(id),cacheName:id);
            if (user == null)
                return StatusCode(404);
            UserEditViewModel uevm = new UserEditViewModel();
            uevm = AutoMapper<User, UserEditViewModel>.Mapping(user, uevm);
            uevm.Password = "";
            return View(uevm);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(UserEditViewModel uevm)
        {
            if(ModelState.IsValid)
            {
                var user = new User();
                user = AutoMapper<UserEditViewModel, User>.Mapping(uevm, user);
                user.Password =Utils.CalculateMD5Hash(user.Password);
                await _repository.Users.Replace(x => x.Id == user.Id, user);
                return RedirectToAction("Index");
            }
            return View(uevm);
        }

        [HttpPost]
        public async Task<IActionResult> DeleteUser(string id)
        {
            if (string.IsNullOrEmpty(id))
                return StatusCode(400);
            await _repository.Users.Remove(x => x.Id == new ObjectId(id));
            return StatusCode(200);
        }


    }
}
