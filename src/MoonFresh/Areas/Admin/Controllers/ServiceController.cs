﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MoonFresh.Core.Areas.Admin;
using MoonFresh.Areas.Admin.ViewModels.Service;
using MongoDB.Bson;
using MoonFresh.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using DbsNetCore.Interfaces;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace MoonFresh.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class ServiceController : Controller
    {
        // GET: /<controller>/
        private ServiceCRUD _service;
        private ServiceCategoryCRUD _servicecategory;
       public ServiceController(IRepository repo)
        {

            _service = new ServiceCRUD(repo, "img/services", repo.Services,40);
            _servicecategory = new ServiceCategoryCRUD(repo, "img/servicescategory", repo.ServiceCategoryes, 40);
        }
        [HttpGet]
        public async  Task<IActionResult> Index()
        {
            
            var model = new ServiceIndexViewModel
            {
                Categoryes = (await _servicecategory.GetAll()).Select(x => (ServiceCategory)x).ToList(),
                Services = (await _service.GetAll()).Select(x => (Service)x).ToList()
            };


            return View(model);
        }

        [HttpGet]
        public IActionResult AddService()
        {
            ViewBag.CategoryId = GetCategories(_servicecategory.GetAll().Result.Select(x => (IModelTitle)x).ToList());
            return View();
        }
        [HttpGet]
        public IActionResult AddServiceCategory()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> AddServiceCategory(ServiceCategoryAddViewModel scavm)
        {
            if (ModelState.IsValid)
            {
                await _servicecategory.AddWithImage(scavm);
                return RedirectToAction("Index");
            }
            return View(scavm);
        }
        [HttpGet]
        public async Task<IActionResult> EditServiceCategory(string id)
        {
            if (string.IsNullOrEmpty(id))
                return BadRequest();
            var model = await _servicecategory.Get(new ObjectId(id));
            if (model == null)
                return NotFound();
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> EditServiceCategory(ServiceCategoryEditViewModel scevm)
        {
            if(ModelState.IsValid)
            {
                await _servicecategory.EditWithImage(scevm);
                return RedirectToAction("Index");
            }
            return View(scevm);
        }
        [HttpPost]
        public async Task<StatusCodeResult> DeleteServiceCategory(string id)
        {
            if(!string.IsNullOrEmpty(id))
            {
                ViewBag.CategoryId = GetCategories(_servicecategory.GetAll().Result.Select(x => (IModelTitle)x).ToList());
                await _servicecategory.DeleteWithSub(new ObjectId(id));
                return  StatusCode(200);
            }
            return StatusCode(400);
        }
        [HttpPost]
        public async Task<IActionResult> AddService(ServiceAddViewModel savm)
        {
              if(!ModelState.IsValid)
            {
                return View(savm);
            }
            await _service.AddWithImage(savm);
            return RedirectToAction("Index");
        }

        [HttpGet]
        public async Task<IActionResult> EditService(string id)
        {

            if (string.IsNullOrEmpty(id))
            {
                return BadRequest();
            }
            ViewBag.CategoryId = GetCategories(_servicecategory.GetAll().Result.Select(x => (IModelTitle)x).ToList());
            var model =await  _service.Get(new ObjectId(id), new ServiceEditViewModel());
            if (model != null)
                return View(model);
            return NotFound();
        }
        [HttpPost]
        public async Task<IActionResult> EditService(ServiceEditViewModel sevm)
        {
            if (!ModelState.IsValid)
                return View(sevm);
            await _service.EditWithImage(sevm);
            return RedirectToAction("Index");
        }

        [HttpPost]
        public async Task<IActionResult> Delete(string id)
        {
            if (string.IsNullOrEmpty(id))
                return StatusCode(400);
            await _service.DeleteWithSub(new ObjectId(id));
            return StatusCode(200);
        }


        protected virtual IList<SelectListItem> GetCategories(IList<IModelTitle> _categories, ObjectId id = new ObjectId())
        {


            List<SelectListItem> lsli = new List<SelectListItem>();
            foreach (var v in _categories)
            {
                bool selected = false;
                if (v.Id == id && id.Pid != 0)
                    selected = true;
                lsli.Add(new SelectListItem() { Text = v.Title, Value = v.Id.ToString(), Selected = selected });
            }

            return lsli;


        }

    }
}
