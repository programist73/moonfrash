﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MoonFresh.Core.Areas.Admin;
using MoonFresh.Models;
using MongoDB.Bson;
using MoonFresh.Areas.Admin.ViewModels.Material;
using DbsNetCore.Interfaces;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace MoonFresh.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class MaterialController :BaseController
    {
        // GET: /<controller>/
       
        MaterialCRUD _mcrud;
        MaterialCategoyCRUD _mccrud;
        List<MaterialCategory> _categories;
        public MaterialController(IRepository repo)
        {

            _mccrud = new MaterialCategoyCRUD(repo, "", repo.MaterialCategoryes, 40);
            _mcrud=new MaterialCRUD(repo, "img/materials/", repo.Materials, 40);
            _categories = _mccrud.GetAll().Result.Select(x=>(MaterialCategory)x).ToList();
            _categories.Add(new MaterialCategory
            {
                Id = new ObjectId("000000000000000000000000"),
                Title = "Все"

            });
        }
        public async Task<IActionResult> Index(int pageNumber = 1, string categoryId = "000000000000000000000000")
        {
            ObjectId id = new ObjectId();
            if (string.IsNullOrEmpty(categoryId))
                id = new ObjectId("000000000000000000000000");
            else
            {
                id = new ObjectId(categoryId);
            }
            ViewBag.categoryId = GetCategories(_categories.Select(x=>(IModelTitle)x).ToList(), id);
            var model = await _mcrud.GetAll(id, pageNumber);

            
            return View(model);
        }

        [HttpGet]
        public IActionResult AddCategory()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> AddCategory(MaterialCategoryAddViewModel mcivm)
        {
            if(!ModelState.IsValid)
            {
                return View(mcivm);
            }
            await _mccrud.Add(mcivm);
            return RedirectToAction("Index");

        }

        [HttpGet]
        public async Task<IActionResult> EditCategory(string id)
        {
            if (string.IsNullOrEmpty(id))
                return BadRequest();
            var model = await _mccrud.Get(new ObjectId(id), new MaterialCategoryEditViewModel());
            if (model == null)
                return NotFound();
            return View((MaterialCategoryEditViewModel)model);

        }
        [HttpPost]
        public async Task<IActionResult> EditCategory(MaterialCategoryEditViewModel mcevm)
        {
            if (!ModelState.IsValid)
                return View(mcevm);
            await _mccrud.Edit(mcevm);
            return RedirectToAction("Index");
        }

        [HttpPost]
        public async Task<IActionResult> DeleteCategory(string id)
        {
            if (string.IsNullOrEmpty(id))
                return BadRequest();
            await _mccrud.DeleteWithSub(new ObjectId(id));
            return StatusCode(200);
        }


        [HttpGet]
        public override IActionResult Add()
        {
            ViewBag.CategoryId = GetCategories(_categories.Select(x => (IModelTitle)x).ToList());
            return base.Add();
        }

        [HttpPost]
        public async Task<IActionResult> Add(MaterialAddViewModel mavm)
        {
           
          

            return await  base.Add(mavm, ( async ()=>
            {
                return await _mcrud.AddWithImage(mavm);
                
            }));
        }

        [HttpGet]
        public async Task<IActionResult> Edit(string id)
        {
          
            return await base.Edit(id, async () =>
             {
                 ViewBag.CategoryId = GetCategories(_categories.Select(x => (IModelTitle)x).ToList(), new ObjectId(id));
                 return await _mcrud.Get(new ObjectId(id), new MaterialEditViewModel());
             });
        } 

        [HttpPost]
        public async Task<IActionResult>Edit(MaterialEditViewModel mevm)
        {
            return await base.Edit(mevm, async () =>
             {
                 await _mcrud.EditWithImage(mevm);
                 return true;
             });
        }

        
        [HttpPost]
        public async Task<IActionResult> Delete(string id)
        {
            int i = 0;

            return await base.Delete(id, async () =>
             {
                 await _mcrud.DeleteWithSub(new ObjectId(id));
             });
        }

    }
}
