﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using MongoDB.Bson;
using DbsNetCore.Inrefaces;
using DbsNetCore.Interfaces;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace MoonFresh.Areas.Admin.Controllers
{
    public class BaseController : Controller
    {

        public IRepository repo;
        public BaseController()
        {

        }
        public BaseController(IRepository repo)
        {
            this.repo = repo;
        }
        

     [HttpGet]
     public  virtual  IActionResult Add()
     {
            return View();
     }


    public virtual async Task<IActionResult> Add(IViewModel ivm, Func<Task<bool>> method)

     {
            if (!ModelState.IsValid)
                return View(ivm);
           await  method();

            return RedirectToAction("Index");
     }
        
     [HttpGet]
     protected virtual async Task<IActionResult>Edit(string id,Func<Task<IViewModel>> method)
        {
            if (string.IsNullOrEmpty(id))
                return BadRequest();
            var model =await  method();
            if (model == null)
                return NotFound();
            return View(model);
        }

        [HttpPost]
        protected virtual async Task<IActionResult> Edit(IViewModel ivm, Func<Task<bool>> method)
        {
            if (!ModelState.IsValid)
                return View(ivm);
            await method();
            return RedirectToAction("Index");
        }





        [HttpPost]
        protected  virtual async Task<IActionResult> Delete(string id,Func<Task> method)
        {
            if (string.IsNullOrEmpty(id))
                return BadRequest();
            await method();
            return StatusCode(200);

        }


        protected virtual IList<SelectListItem> GetCategories(IList<IModelTitle> _categories,ObjectId id = new ObjectId())
        {


            List<SelectListItem> lsli = new List<SelectListItem>();
            foreach (var v in _categories)
            {
                bool selected = false;
                if (v.Id == id && id.Pid != 0)
                    selected = true;
                lsli.Add(new SelectListItem() { Text = v.Title, Value = v.Id.ToString(), Selected = selected });
            }

            return lsli;


        }

    }
}
