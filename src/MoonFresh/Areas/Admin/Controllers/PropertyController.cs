﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MoonFresh.Models;
using MoonFresh.Areas.Admin.ViewModels.Property;
using DbsNetCore.Utils;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace MoonFresh.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class PropertyController :BaseController
    {
        private readonly IRepository _repository;
        private const string _Url = "/admin/Testimonial/";
        private const string _PathToImage = "img/Social/";

        public PropertyController(IRepository repository)
        {
            _repository = repository;


        }

        // GET: /<controller>/
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            var property = await _repository.Propertys.GetOne(x => x.Name == "Шаблон", cacheName:"Шаблон");

            return View(property);
        }

       

        [HttpPost]
        public async Task<IActionResult> Add(PropertyAddViewModel pavm)
        {

            return await base.Add(pavm, async () =>
            {

                                                                                   

                Property property = AutoMapper<PropertyAddViewModel, Property>.Mapping(pavm, new Property());
                if (pavm.Logo != null)
                {
                    var result = _repository.Files.Upload(pavm.Logo, pavm.Logo.FileName);
                    property.PathToLogo = result.Name;
                }
                if(pavm.Image!=null)
                {
                    var result = _repository.Files.Upload(pavm.Image, pavm.Image.FileName);
                    property.About.PathToImage = result.Name;
                }

                         
                await _repository.Propertys.Save(property);
                return true;

            });
         

        }


        [HttpGet]
        public async Task<IActionResult> Edit(string name)
        {
            return await base.Edit(name, async () =>
            {
                var itm = await _repository.Propertys.GetOne(x => x.Name == name, cacheName: name);
                PropertyEditViewModel pevm = new PropertyEditViewModel();
                pevm = AutoMapper<Property, PropertyEditViewModel>.Mapping(itm, pevm);
                
                return pevm;
            });

            }


        [HttpPost]
        public async Task<IActionResult> Edit(PropertyEditViewModel pevm)
        {
            return await base.Edit(pevm, async () =>
            {
                Property property = new Property();
                property = AutoMapper<PropertyEditViewModel, Property>.Mapping(pevm, property);
                           
              
                if(pevm.NewSocial!=null)
                foreach (var itm in pevm.NewSocial)
                {
                    if (!string.IsNullOrEmpty(itm.Name))
                        property.Social.Add(itm);
                }
                if(pevm.NewPhone!=null)
                foreach(var itm in pevm.NewPhone)
                {
                    property.Phone.Add(itm);
                }
                if (pevm.Logo != null)
                {
                    _repository.Files.ReplaceFile(property.PathToLogo, pevm.Logo);
                }
                if (pevm.Image != null)
                {
                    _repository.Files.ReplaceFile(property.About.PathToImage, pevm.Image);
                }
               var geo= await Utils.GoogleGeocode(pevm.Addres);
                property.Lat = geo.results[0].geometry.location.lat;
                property.Long = geo.results[0].geometry.location.lng;
                await _repository.Propertys.Replace(x => x.Id == property.Id, property);
                return true;

            });




        }


    }
}
