﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;
using DbsNetCore.Models.Identity;
using DbsNetCore.Utils;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace MoonFresh.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize]
    public class AccountController : Controller
    {

        private readonly IRepository _repository;

        public AccountController(IRepository repository)
        {

            _repository = repository;

        }

        // GET: /<controller>/
        [Authorize]
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Login(string ReturnUrl)
        {
            ViewBag.ReturnUrl = ReturnUrl;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken()]
        public async Task<IActionResult> Login(User user, string ReturnUrl)
        {
            if (ModelState.IsValid)
            {
                var usersing = await _repository.Users.GetOne(x => x.UserName == user.UserName, cacheName: user.UserName);
                if (usersing == null)
                    return StatusCode(404);
                string pwd = Utils.CalculateMD5Hash(user.Password);


                if (pwd == usersing.Password)
                {
                    IList<Claim> claimCollection = new List<Claim>
                {
                    new Claim(ClaimTypes.Name,"Admin"),
                    new Claim(ClaimTypes.Country,"Sweden"),
                    new Claim(ClaimTypes.Gender,"M"),
                    new Claim(ClaimTypes.Surname,"Nemes"),
                    new Claim(ClaimTypes.Email,usersing.UserName),
                    new Claim(ClaimTypes.Role,"Admin")
                };

                    var claimsIdentity = new ClaimsIdentity(claimCollection, "Password");


                    if (claimsIdentity.IsAuthenticated)
                    {
                        var principal = new ClaimsPrincipal(claimsIdentity);

                        await HttpContext.Authentication.SignInAsync("MyCookieMiddlewareInstanceAuth", principal);
                    }
                    return Redirect(ReturnUrl);
                }
                else
                {
                    ModelState.AddModelError("LogOnError", "The user name or password provided is incorrect.");
                    return View(user);
                }
            }
            else
                return View(user);

        }

        [HttpGet]

        public async Task<IActionResult> Logout()
        {

            await HttpContext.Authentication.SignOutAsync("MyCookieMiddlewareInstanceAuth");
            return RedirectToAction("Login");



        }





    }
}
