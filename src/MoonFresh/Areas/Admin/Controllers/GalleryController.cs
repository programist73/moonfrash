﻿
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MoonFresh.Models;
using MongoDB.Bson;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Linq;
using MoonFresh.Areas.Admin.ViewModels.Gallery;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace MoonFresh.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class GalleryController : Controller
    {
        public readonly    IRepository _db;
        private const int _take = 40;
        private Core.Areas.Admin.GalleryCRUD _gallery;
        private List<GalleryCategory> _categories;

        public GalleryController(IRepository reposytory)
        {
            _gallery = new Core.Areas.Admin.GalleryCRUD(reposytory, "img/gallery/",reposytory.GalleryCategoryes,40);
            var cat = _gallery.GetAll().Result;
            _categories = cat.Select(x => (GalleryCategory)x).ToList();
            _categories.Add(new GalleryCategory
            {
                Id = new ObjectId("000000000000000000000000"),
                Title = "Все"

            });
          
            
        }

        // GET: /<controller>/
        [HttpGet]
        public async Task<IActionResult> Index(int pageNumber=1,string categoryId= "000000000000000000000000")
        {
            
            
            ObjectId id = new ObjectId();
            if (string.IsNullOrEmpty(categoryId))
                id = new ObjectId("000000000000000000000000");
            else
            {
                id = new ObjectId(categoryId);
            }
            ViewBag.categoryId = GetCategories(id);
            var model = await _gallery.GetAllImages(id, pageNumber);
            
            return View(model);
        }

        [HttpGet]
        public IActionResult AddGalleryCategory()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> AddGalleryCategory(GalleryCategoryAddViewModel gcavm)
        { 
            if(!ModelState.IsValid )
            {
                return View(gcavm);
            }

          var result=  await _gallery.AddWithImage(gcavm);
            if (result == false)
                return BadRequest();

            return RedirectToAction("Index");

        }

        [HttpGet]
        public async Task<IActionResult> EditGalleryCategory(string id)
        {
            if (string.IsNullOrEmpty(id))
                return BadRequest();
                var model =await  _gallery.Get(new ObjectId(id), new GalleryCategoryEditViewModel());
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> EditGalleryCategory(GalleryCategoryEditViewModel gcevm)
        {
            if (ModelState.IsValid)
            {
                await _gallery.EditWithImage(gcevm);
            }

            return RedirectToAction("Index");

        }

        [HttpPost]
        public IActionResult DeleteGalleryCategory(string id)
        {
            if (!string.IsNullOrEmpty(id))
            { var res = _gallery.DeleteWithSub(new ObjectId(id)).Result; }
            return StatusCode(200);
        }

        [HttpGet]
        public IActionResult AddGallery()
        {
            SelectList si = new SelectList(_categories, "Id", "Title");
            ViewBag.CategoryId = si;
            return View();
        }

        [HttpPost]
        public async  Task<IActionResult> AddGallery(GalleryAddViewModel gavm)
        {
            if(!ModelState.IsValid)
            {
                return View(gavm);
            }
            await _gallery.GalleryAdd(gavm);
            return RedirectToAction("Index");

        }

        [HttpGet]
        public async Task<IActionResult> EditGallery(string id)
        {
            if (string.IsNullOrEmpty(id))
                return BadRequest();
            
            var gallery = await _gallery.GetGallery(new ObjectId(id));
            ViewBag.CategoryId = GetCategories(new ObjectId(gallery.CategoryId));
            return View(gallery);

        }
        [HttpPost]
        public async Task<IActionResult> EditGallery(GalleryEditViewModel gevm)
        {
            if (!ModelState.IsValid)
                return View(gevm);
            await _gallery.EditGallery(gevm);
            return RedirectToAction("Index");
        }

        [HttpPost]
        public IActionResult Delete(string id)
        {
            if(string.IsNullOrEmpty(id))
            {
                return BadRequest();
            }
            var res=_gallery.DeleteGallery(new ObjectId(id)).Result;
            return StatusCode(200);
        }


        protected IList<SelectListItem> GetCategories(ObjectId id = new ObjectId())
        {


            List<SelectListItem> lsli = new List<SelectListItem>();
            foreach (var v in _categories)
            {
                bool selected = false;
                if (v.Id == id && id.Pid != 0)
                    selected = true;
                lsli.Add(new SelectListItem() { Text = v.Title, Value = v.Id.ToString(), Selected = selected });
            }

            return lsli;


        }

    }
}
