﻿using DbsNetCore.Inrefaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.IO;

namespace MoonFresh.Core
{
    public class Core
    {

       private  string _Url, _PathToImage;
       
        public string Url
        {
            set { _Url = value; }
            get { return _Url; }
        }

        public string PathToImage
        {
            set
            {
                _PathToImage = value;
           }
            get
            {
                return _PathToImage;
            }
        }

        public Core(string pathToImage)
        {
            _PathToImage = pathToImage;
        }


        public string SaveFile(IRepository repo,IFormFile file)
        {
            try
            {
                if (file != null)
                {
                    string path = _PathToImage + file.FileName;
                    var result = repo.Files.Upload(file, path);
                    return result.Name;
                }
                else
                {
                    throw new NullReferenceException("File must be not null!!!");
                }
            }
            catch(Exception e)
            {
                throw new Exception(e.Message);
            }

        }

        public IViewModelImage ReplaceFile(IRepository repo, IViewModelImage viewModel)
        {
            try
            {

            
            if (viewModel.Image != null)
            {
                    if(viewModel.PathToImage!=null)
                repo.Files.Delete(viewModel.PathToImage);
                viewModel.PathToImage = SaveFile(repo, viewModel.Image);
                return viewModel;
            }
            else return null;
            }
            catch(Exception e)
            {
                throw new Exception(e.Message);
            }

        }

        public IActionResult ReadFile(IRepository repo,string fileName)
        {
            byte[] bt;
            string contentType = "";
            var stream = repo.Files.DownloadFileToStream(fileName, out contentType);
            if (stream == null || stream.CanRead == false)
                return null;
            using (MemoryStream ms = (MemoryStream)stream)
            {
                stream.CopyTo(ms);
                bt = ms.ToArray();
            }
            return new FileStreamResult(new MemoryStream(bt), contentType);
        }



    }
}
