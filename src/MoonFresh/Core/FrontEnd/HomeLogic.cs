﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MoonFresh.Core.Areas.Admin;
using MoonFresh.Models;
using MoonFresh.ViewModels.Home;
using MongoDB.Bson;

namespace MoonFresh.Core.FrontEnd
{
    public class HomeLogic
    {

        private IRepository repo;
        private GalleryCRUD GalleryCrud { get; set; }
        private MaterialCategoyCRUD MaterialCategoryCrud { get; set; }
        private MaterialCRUD MaterialCrud { get; set; }
        private ServiceCRUD ServiceCrud { get; set; }
        ServiceCategoryCRUD ServiceCategoryCrud { get; set; }
        private PropertyCRUD PropertyCrud { get; set; }

        public HomeLogic(IRepository repo)
        {
            GalleryCrud = new GalleryCRUD(repo, "", repo.GalleryCategoryes, 30);
            MaterialCategoryCrud = new MaterialCategoyCRUD(repo, "", repo.MaterialCategoryes, 30);
            MaterialCrud = new MaterialCRUD(repo, "", repo.Materials, 30);
            ServiceCrud = new ServiceCRUD(repo, "", repo.Services, 30);
            ServiceCategoryCrud = new ServiceCategoryCRUD(repo, "", repo.ServiceCategoryes, 30);
            PropertyCrud = new PropertyCRUD(repo, "", repo.Propertys, 30);
            this.repo = repo;

        }

        public async Task<List<GalleryCategory>> GetAllGalleryCategory()
        {

            return (await GalleryCrud.GetAll()).Select(x => (GalleryCategory)x).ToList();
        }

        public async Task<GalleryViewModel> GetGallery(ObjectId id)
        {
           
            var model = new GalleryViewModel
            {
                Category = (await GalleryCrud.Get(id)) as GalleryCategory,
                Gallerys = await repo.Galleres.Get(x => x.CategoryId == id, cacheName: "galleryCategoru" + id.ToString())
            };
            return model;
        }

        public async Task<GalleryViewModel> GetGallery(string id)
        {

            var model = new GalleryViewModel
            {
                Category = await repo.GalleryCategoryes.GetOne(x=>x.Title==id,cacheName:"galleryCatTitile"+id)
               
            };
            model.Gallerys = await repo.Galleres.Get(x => x.CategoryId == model.Category.Id, cacheName: "galleryCategoru" + id.ToString());
            return model;
        }
        public async Task<MaterialAllViewModel> GetAllMaterial(ObjectId id)
        {
            var MaterialCat =await MaterialCategoryCrud.Get(id);
            var model =await  MaterialCrud.GetAll();
            MaterialAllViewModel mvm = new MaterialAllViewModel
            {
                CategoryTitle = ((MaterialCategory)MaterialCat).Title,
                Material=model.Select(x => (Material)x).Where(x => x.CategoryId == id).ToList()
            };
            return mvm;
        }

       public async Task<MaterialSingleViewModel> GetSingleMaterial(ObjectId id)
        {
            var Material = await MaterialCrud.Get(id) as Material;
            var model = new MaterialSingleViewModel
            {
                MaterialCategory = await MaterialCategoryCrud.Get(Material.CategoryId) as MaterialCategory,
                Material = new MoreViewModel
                {
                    Title = Material.Title,
                    Text = Material.Text
                }

        };
            return model;

        }

        public async Task<List<Service>> GetServices()
        {
            var model = await ServiceCrud.GetAll();
            return model.Select(x => (Service)x).ToList();
        }
        public async Task<List<Service>> GetServices(ObjectId categoryId)
        {
            var model = await repo.Services.Get(x => x.CategoryId == categoryId, cacheName: "serviceCategoryId" + categoryId.ToString());
            return model;
        }



        public async Task<Service> GetService(ObjectId id)
        {
            var model = await ServiceCrud.Get(id);
            return (Service)model;
        }

        public async Task<IList<ServiceCategory>> GetServiceCategories()
        {
            var model = await ServiceCategoryCrud.GetAll();
            return model.Select(x=>(ServiceCategory)x).ToList();
        }

        public async Task<MoreViewModel> MoreService(string id)
        {
            var model = await GetService(new ObjectId(id));
            return new MoreViewModel
            {
                Text = model.More,
                Title = model.Title
            };
        }

        public async Task<Property> GetProperty()
        {
            var model = await PropertyCrud.GetAll();
            return model.SingleOrDefault() as Property;
        }
        public async Task<MoreViewModel> MoreAbout()
        {
            var model =await  GetProperty();
            return new MoreViewModel
            {
                Title = model.About.Title,
                Text = model.About.More
            };
        }

        
     

    }
}
