﻿using MoonFresh.Models;
using MoonFresh.ViewModels.Home;
using System.Linq;
using System.Threading.Tasks;

namespace MoonFresh.Core.FrontEnd
{
    public class PropertyCRUD : BaseCRUD<Property>
    {
        public PropertyCRUD(IRepository repo, string pathToImage, DbsNetCore.Interfaces.IDataBase<Property> db, int pageSize) : base(repo, pathToImage, db, pageSize)
        {
        }

       
       public async Task<IndexViewModel> GetProperty()
        {
            var model = await GetAll();
            if (model != null)
                return new IndexViewModel
                {
                    Property = (Property)(model.SingleOrDefault())
                };
            return null;
        }

        

    }
}
