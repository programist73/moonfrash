﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DbsNetCore.Interfaces;
using DbsNetCore.Utils;
using DbsNetCore.Inrefaces;

namespace MoonFresh.Core
{
    public class BaseCRUD<T> where T :class, new()
    {

        protected Core _core;
        protected IRepository repo;
        protected string _pathToImage;
        protected IDataBase<T> _db;
        protected  int _pageSize;


        public    BaseCRUD(IRepository repo, string pathToImage, IDataBase<T> db,int pageSize)
        {
            this.repo = repo;
            _pathToImage = pathToImage;
            _core = new Core(_pathToImage);
            _db = db;
            _pageSize = pageSize;



        }

        public async Task<IList<IModel>> GetAll()
        {
            try
            {
                var galleryCat = await _db.Get();
                List<IModel> lst = new List<IModel>();
                lst.AddRange(galleryCat.Select(x => (IModel)x));
                return lst;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        public async Task<IModel> Get(ObjectId id)
        {
            try
            {
                var galleryCat = await _db.Get(x => ((IModel)x).Id == id,cacheName:id.ToString());


                return (IModel)galleryCat.Single();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }


        public virtual async Task<IViewModel> Get(ObjectId id,IViewModel ivm)
        {
            try
            {
                var galleryCat = await _db.Get(x => ((IModel)x).Id == id);

                var model = AutoMapper<T, IViewModel>.Mapping(galleryCat.Single(), ivm);
                return model;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }


        public async Task<bool> Add(IViewModel gcavm)
        {
            try
            {
                var model = AutoMapper<IViewModel, T>.Mapping(gcavm, new T());
                await _db.Save(model);
                return true;

            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

        }

        public async Task<bool> AddWithImage(IViewModelImage gcavm)
        {
            try
            {
                

                string path = gcavm.Image != null?_core.SaveFile(repo, gcavm.Image):null;
                if (path != null)
                    gcavm.PathToImage = path;

                var model = AutoMapper<IViewModelImage, T>.Mapping(gcavm, new T());
                await _db.Save(model);
                return true;

            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

        }


        public async Task Edit(IViewModel gcevm)
        {
            try
            {


                var model = AutoMapper<IViewModel, T>.Mapping(gcevm, new T());

                await _db.Replace(x => ((IModel)x).Id == ((IModel)model).Id, model);


            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

        }


        public async Task EditWithImage(IViewModelImage gcevm)
        {
            try
            {
                if(gcevm.Image!=null)
                gcevm = _core.ReplaceFile(repo, gcevm);

                var model = AutoMapper<IViewModelImage, T>.Mapping(gcevm, new T());

                await _db.Replace(x => ((IModel)x).Id == ((IModel)model).Id, model);


            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

        }



        public async Task<bool> DeleteWithSub(ObjectId id)
        {
            try
            {
                if (id != null)
                {
                    var category = await Get(id);
                    if(category is IModelWithImage)
                    {
                        if(!string.IsNullOrEmpty(((IModelWithImage)category).PathToImage))
                        repo.Files.Delete(((IModelWithImage)category).PathToImage);
                    }
                    await RemoveSub(id);
                    await _db.Remove(x => ((IModel)x).Id == id);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

        }


        public async Task<bool> Delete(ObjectId id)
        {
            try
            {
                if (id != null)
                {
                    await _db.Remove(x => ((IModel)x).Id == id);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

        }


        protected virtual async Task RemoveSub(ObjectId categoryId)
        {
            

        }








    }
}
