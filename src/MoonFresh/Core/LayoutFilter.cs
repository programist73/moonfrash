﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using MoonFresh.Controllers;
using MoonFresh.Core.FrontEnd;
using MoonFresh.Areas.Admin.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoonFresh.Core
{
    public class LayoutFilter : Attribute, IActionFilter
    {
        
        //public MyActionFilter(IRepository repo)
        //{
        //    _home = new Home(repo, null, repo.Propertys, 0);
        //}

        public void OnActionExecuted(ActionExecutedContext context)
        {
        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
            var myController = context.Controller as BaseController;

            if (myController != null)
            {

                var property = myController.repo.Propertys.Get().Result.SingleOrDefault();
                var galleryCat = myController.repo.GalleryCategoryes.Get().Result;
                var materialCategory = myController.repo.MaterialCategoryes.Get().Result;
                myController.ViewBag.materialCategory = materialCategory;
                myController.ViewBag.GalleryCat = galleryCat;
                myController.ViewBag.Property = property;
            }
        }
    }
}
