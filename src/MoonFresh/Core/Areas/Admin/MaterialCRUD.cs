﻿using MoonFresh.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Bson;
using MoonFresh.Areas.Admin.ViewModels.Material;
using DbsNetCore.Interfaces;
using DbsNetCore.MongoDB;
using DbsNetCore.Utils.Helpers.Models;

namespace MoonFresh.Core.Areas.Admin
{
    public class MaterialCRUD:BaseCRUD<Material>

    {
        public MaterialCRUD(IRepository repo,string pathToImage,IDataBase<Material> db,int pageSize ):base(repo,pathToImage,db,pageSize)
        {

        }

        public async Task<MaterialIndexViewModel> GetAll(ObjectId categoryId, int pageNumber = 0)
        {
            TotalItem<List<Material>> model;

            if (!(categoryId.ToString() == "000000000000000000000000"))
                model = await _db.Get(_pageSize * (pageNumber - 1), _pageSize, x => x.CategoryId == categoryId, cacheName: categoryId.ToString());
            else
                model = await _db.Get(_pageSize * (pageNumber - 1), _pageSize);

            var images = model;

            MaterialIndexViewModel givm = new MaterialIndexViewModel
            {
                PageInfo = new PageInfo
                {
                    PageNumber = pageNumber,
                    PageSize = _pageSize,
                    TotalItem = images.TotalItemCount,
                    Url = "/admin/material",
                    Parametrs = !(categoryId.ToString() == "000000000000000000000000") ? "&categoryId=" + categoryId.ToString() : "&categoryId="
                },
               
           };
            givm.Materials = images.Result;
            givm.MaterialCategoryes = await repo.MaterialCategoryes.Get();

            return givm;
        }


        protected override async Task RemoveSub(ObjectId materialId)
        {
            var model = await _db.GetOne(x => x.Id == materialId, cacheName: materialId.ToString());
            if(model.PathToImage!=null)
            repo.Files.Delete(model.PathToImage);
            await   _db.Remove(x => x.Id == materialId);


            
        }
    }
}
