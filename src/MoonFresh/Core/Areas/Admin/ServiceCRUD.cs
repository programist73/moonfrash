﻿using System.Threading.Tasks;
using MongoDB.Bson;
using DbsNetCore.Interfaces;
using System.Collections.Generic;
using MoonFresh.Models;

namespace MoonFresh.Core.Areas.Admin
{
    public class ServiceCRUD:BaseCRUD<Models.Service>
    {
        public ServiceCRUD(IRepository repo,string pathToImage,IDataBase<Models.Service> db,int pageSize ):base(repo,pathToImage,db,pageSize)
        {

        }

   

        public async Task<List<Service>> GetServices(ObjectId categoryId)
        {
            var model = await _db.Get(x => x.CategoryId == categoryId,cacheName:categoryId.ToString());
            return model;
        }
        
        protected override async  Task RemoveSub(ObjectId Id)
        {
            var model = await _db.GetOne(x => x.Id == Id, cacheName: Id.ToString());
            if(!string.IsNullOrEmpty(model.PathToImage))
            repo.Files.Delete(model.PathToImage);


        }
    }
}
