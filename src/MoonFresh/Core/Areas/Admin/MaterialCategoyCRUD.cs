﻿using MoonFresh.Models;
using System.Threading.Tasks;
using MongoDB.Bson;
using DbsNetCore.Interfaces;

namespace MoonFresh.Core.Areas.Admin
{
    public class MaterialCategoyCRUD:BaseCRUD<MaterialCategory>
    {
        public MaterialCategoyCRUD(IRepository repo,string pathToImage,IDataBase<MaterialCategory> db,int pageSize):base(repo,pathToImage,db,pageSize)
        {

        }

        protected override async Task RemoveSub(ObjectId categoryId)
        {
            var model = await repo.Materials.Get(x => x.CategoryId == categoryId);
            foreach (var item in model)
                repo.Files.Delete(item.PathToImage);
            await repo.Materials.RemoveMany(x => x.CategoryId == categoryId);

        }
    }
}
