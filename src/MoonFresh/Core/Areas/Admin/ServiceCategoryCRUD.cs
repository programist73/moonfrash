﻿using DbsNetCore.Interfaces;
using MongoDB.Bson;
using MoonFresh.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoonFresh.Core.Areas.Admin
{
    public class ServiceCategoryCRUD:BaseCRUD<ServiceCategory>
    {
        public ServiceCategoryCRUD(IRepository repo, string pathToImage, IDataBase<ServiceCategory> db, int pageSize):base(repo,pathToImage,db,pageSize)
        {

        }


        protected override async Task RemoveSub(ObjectId Id)
        {
            var model = await _db.GetOne(x => x.Id == Id, cacheName: Id.ToString());
            repo.Files.Delete(model.PathToImage);
            var services = await repo.Services.Get(x => x.CategoryId == Id,cacheName:Id.ToString());
            await repo.Services.RemoveMany(x => x.CategoryId == Id);
            Parallel.ForEach(services, x =>
            {
                repo.Files.Delete(x.PathToImage);
            });


        }

    }
}
