﻿using System;
using System.Threading.Tasks;
using MongoDB.Bson;
using MoonFresh.Models;
using System.Linq;
using MoonFresh.Areas.Admin.ViewModels.Gallery;
using DbsNetCore.Interfaces;
using DbsNetCore.Utils;
using DbsNetCore.Inrefaces;
using DbsNetCore.Utils.Helpers.Models;

namespace MoonFresh.Core.Areas.Admin
{
    public sealed class  GalleryCRUD:BaseCRUD<GalleryCategory>
    {
        public GalleryCRUD(IRepository repo, string pathToImage, IDataBase<GalleryCategory> db,int pageSize)
            :base(repo,pathToImage,db,pageSize)
        {

        }

        public async Task<GalleryIndexViewModel> GetAllImages(ObjectId categoryId,int pageNumber=0)
        {
            dynamic model;

            if (!(categoryId.ToString()== "000000000000000000000000"))
            model = await repo.Galleres.Get(_pageSize * (pageNumber - 1),_pageSize,x=> x.CategoryId == categoryId,cacheName:categoryId.ToString());
            else
                model = await repo.Galleres.Get(_pageSize * (pageNumber - 1), _pageSize);

            var images = model;

            GalleryIndexViewModel givm = new GalleryIndexViewModel
            {
                PageInfo = new PageInfo
                {
                    PageNumber = pageNumber,
                    PageSize = _pageSize,
                    TotalItem = images.TotalItemCount,
                    Url = "/admin/gallery",
                    Parametrs = !(categoryId.ToString() == "000000000000000000000000") ? "&categoryId=" + categoryId.ToString() : "&categoryId="
                },
                Galleres = images.Result,
                GalleryCategories = GetAll().Result.Select(x => (GalleryCategory)x).ToList()
            };

            return givm;
        } 

        public async Task<bool> GalleryAdd(IViewModelImage gavm)
        {
            try
            {

                string path = _core.SaveFile(repo, gavm.Image);
                if (gavm.Image != null)
                    gavm.PathToImage = path;
                var model = AutoMapper<IViewModelImage, MoonFresh.Models.Gallery>.Mapping(gavm, new MoonFresh.Models.Gallery());
                await repo.Galleres.Save(model);
                return true;
                

            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        
        public async Task<GalleryEditViewModel> GetGallery(ObjectId id)
        {
            try
            {
                var gallery = await repo.Galleres.Get(x => x.Id == id);
                var model = AutoMapper<Models.Gallery, GalleryEditViewModel>.Mapping(gallery.Single(), new GalleryEditViewModel());
                return model;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        } 

        public async Task<bool> EditGallery(GalleryEditViewModel gevm)
        {
            try
            {
               
                var model = AutoMapper<GalleryEditViewModel, Models.Gallery>.Mapping(gevm, new Models.Gallery());
                if (gevm.Image != null)
                {
                    var path = _core.ReplaceFile(repo, gevm);
                    model.PathToImage = path.PathToImage;
                }
                await repo.Galleres.Replace(x => x.Id == model.Id, model);
                return true;
            }
            catch(Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public async Task<bool> DeleteGallery(ObjectId id)
        {
            
            try
            {
                var model = await repo.Galleres.Get(x=>x.Id==id);
                repo.Files.Delete(model.Single().PathToImage);
                await repo.Galleres.Remove(x => x.Id == id);
                return true;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        

         protected override async Task  RemoveSub(ObjectId categoryId)
        {

            try
            {

               
                var Gallery = await repo.Galleres.Get(x => x.CategoryId == categoryId);

                foreach (var item in Gallery)
                {

                    repo.Files.Delete(item.PathToImage);
                    await repo.Galleres.Remove(x => x.Id == item.Id);
                }

            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

        }

     


    }
}
