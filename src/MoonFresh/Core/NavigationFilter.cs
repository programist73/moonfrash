﻿using Microsoft.AspNetCore.Mvc.Filters;
using MoonFresh.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoonFresh.Core
{
    public class NavigationFilter : Attribute, IActionFilter
    {

        public void OnActionExecuted(ActionExecutedContext context)
        {
        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
            var myController = context.Controller as HomeController;

            if (myController != null)
            {

                var navigation = myController._navigation;

                myController.ViewBag.Navigation = navigation;
            }
        }

    }
}
