﻿using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Options;
using MoonFresh.Models;
using DbsNetCore.MongoDB;
using DbsNetCore.Models.Identity;
using System;

namespace MoonFresh
{
    public class Repository:IRepository
    { 
   

    public Repository(IOptions<Settings> settings, IMemoryCache cache)
    {
        Settings setting = settings.Value;
        Users = new MongoHelper<User>(setting, cache);
        GalleryCategoryes=new MongoHelper<GalleryCategory>(setting,cache);
        Galleres = new MongoHelper<Gallery>(setting, cache);
        ServiceCategoryes = new MongoHelper<ServiceCategory>(setting, cache);
        Services = new MongoHelper<Service>(setting, cache);
        Materials = new MongoHelper<Material>(setting, cache);
        MaterialCategoryes = new MongoHelper<MaterialCategory>(setting, cache);
        Propertys = new MongoHelper<Property>(setting, cache);
        Files = new Files(setting);

    }

        public Files Files { get; set; }
        public MongoHelper<User> Users { get; set; }
        public MongoHelper<GalleryCategory> GalleryCategoryes { get; set; }
        public MongoHelper<Gallery> Galleres { get; set; }
        public MongoHelper<Service> Services { get; set; }
        public MongoHelper<Material> Materials { get; set; }
        public MongoHelper<MaterialCategory> MaterialCategoryes { get; set; }
        public MongoHelper<Property> Propertys { get; set; }
        public MongoHelper<ServiceCategory> ServiceCategoryes { get ; set; }
    }


    
    

}

