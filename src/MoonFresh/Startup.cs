﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Net.Http.Headers;
using DbsNetCore.MongoDB;
using Microsoft.Extensions.WebEncoders;
using System.Text.Encodings.Web;
using System.Text.Unicode;

namespace MoonFresh
{
    public class Startup
    {

        public Startup()
        {
            var builder =
              new ConfigurationBuilder()
              .SetBasePath(System.IO.Directory.GetCurrentDirectory())
              .AddJsonFile("config.json");
            Configuration = builder.Build();
        }

        public IConfiguration Configuration { get; set; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit http://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<WebEncoderOptions>(options =>
            {
                options.TextEncoderSettings = new TextEncoderSettings(UnicodeRanges.All);
            });

            services.AddMvc();
         //   services.AddWebMarkupMin(
         //options =>
         //{
         //    options.AllowMinificationInDevelopmentEnvironment = true;
         //    options.AllowCompressionInDevelopmentEnvironment = true;
         //    options.DisableCompression = false;
         //    options.DisableMinification = false;
         //    options.DisablePoweredByHttpHeaders = true;

         //})
         //   .AddHtmlMinification(options =>
         //   {


         //       HtmlMinificationSettings settings = options.MinificationSettings;
         //       settings.RemoveRedundantAttributes = true;
         //       settings.RemoveHttpProtocolFromAttributes = true;
         //       settings.RemoveHttpsProtocolFromAttributes = true;

         //       options.CssMinifierFactory = new KristensenCssMinifierFactory();
         //       options.JsMinifierFactory = new CrockfordJsMinifierFactory();


         //   })
         //       .AddXhtmlMinification(options =>
         //       {

         //           XhtmlMinificationSettings settings = options.MinificationSettings;
         //           settings.RemoveRedundantAttributes = true;
         //           settings.RemoveHttpProtocolFromAttributes = true;
         //           settings.RemoveHttpsProtocolFromAttributes = true;

         //           options.CssMinifierFactory = new KristensenCssMinifierFactory();
         //           options.JsMinifierFactory = new CrockfordJsMinifierFactory();
         //       })
         //       .AddXmlMinification(options =>
         //       {
         //           XmlMinificationSettings settings = options.MinificationSettings;
         //           settings.CollapseTagsWithoutContent = true;
         //       })

         //.AddHttpCompression(options =>

         //{
         //    options.CompressorFactories = new List<ICompressorFactory>
         //           {

         //               new GZipCompressorFactory(new GZipCompressionSettings
         //               {
         //                   Level = CompressionLevel.Fastest
         //               })
         //           };
         //}

         //       );


            services.AddCors(o => o.AddPolicy("MyPolicy", builder =>
            {
                builder.AllowAnyOrigin()
                       .AllowAnyMethod()
                       .AllowAnyHeader();
            }));

            services.AddOptions();
            services.Configure<RazorViewEngineOptions>(options =>
            {
                options.AreaViewLocationFormats.Clear();
                //0 page name
                //1 controller name
                //2 area name
                options.AreaViewLocationFormats.Add("/Areas/{2}/Views/{1}/{0}.cshtml");
                options.AreaViewLocationFormats.Add("Areas/{2}/Views/Shared/{0}.cshtml");

            });
            services.Configure<Settings>(x =>
            {
                x.MongoConnection = Configuration["Data:MongoConnection"];
                x.Database = Configuration["Data:Database"];
                x.UserName = Configuration["Data:UserName"];
                x.Password = Configuration["Data:Password"];
                x.Port = int.Parse(Configuration["Data:Port"]);
                x.AuthorizeDb = Configuration["Data:AuthorizeDb"];


            });


            services.AddMemoryCache();
            services.AddSingleton<IRepository, Repository>();


        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole();


            app.UseCookieAuthentication(new CookieAuthenticationOptions()
            {
                AuthenticationScheme = "MyCookieMiddlewareInstanceAuth",
                LoginPath = new PathString("/admin/Account/Login/"),
                AccessDeniedPath = new PathString("/Admin/Account/Forbidden/"),
                AutomaticAuthenticate = true,
                AutomaticChallenge = true
            });

            if (env.IsDevelopment())
            {

                app.UseDeveloperExceptionPage();
            }

            app.UseExceptionHandler("/Home/Error");


            //app.UseWebMarkupMin();
            app.UseDefaultFiles();

            app.UseStaticFiles(new StaticFileOptions
            {
                OnPrepareResponse = ctx =>
                {
                    const int durationInSeconds = 60 * 60 * 24;
                    ctx.Context.Response.Headers[HeaderNames.CacheControl] =
                        "public,max-age=" + durationInSeconds;
                }
            });
            app.UseMvc(routs =>
            {

                routs.MapRoute(name: "areaRoute",
                    template: "{area:exists}/{controller=Material}/{action=Index}"
                    );

                routs.MapRoute(
                name: "default",
                template: "{controller=Home}/{action=Index}/{id?}"
                );
                routs.MapRoute("front", "ajax/{action}/{id?}",
            defaults: new { controller = "Home", action = "index" });

                routs.MapRoute("front1", "{action}/{id?}",
           defaults: new { controller = "Home", action = "GalleryAll" });

        //        routs.MapRoute("frontG", "gallery/{id?}",
        //defaults: new { controller = "Home", action = "GalleryAll" });
                //           routs.MapRoute("frontGA", "ajax/gallery/{id?}",
                //defaults: new { controller = "Home", action = "GalleryAll" });
            }

            );
        }
    }
}
