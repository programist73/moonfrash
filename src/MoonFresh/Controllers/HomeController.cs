﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MoonFresh.Core.FrontEnd;
using MoonFresh.Core;
using MoonFresh.Areas.Admin.Controllers;
using MongoDB.Bson;
using System.Collections.Generic;
using System.Linq;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace MoonFresh.Controllers
{

    public class HomeController:BaseController
    {
        public List<NavigationViewModel> _navigation { get; set; }
        private static  IRepository _repo;
        private static HomeLogic _hl;
        private Core.Core _core;
        public HomeController(IRepository repo):base(repo)
        {            
            _repo = repo;
            _core = new Core.Core("");
            _hl = new HomeLogic(_repo);
            _navigation = new List<NavigationViewModel>();
            AddNavigation("", "/", "Главная");

        }

        // GET: /<controller>/
        [HttpGet]
        [LayoutFilter]
        [NavigationFilter]
        public async Task<IActionResult> Index()
        {
            //var navigation = new List<NavigationViewModel>
            //{
            //    new NavigationViewModel
            //    {
            //        Id="",
            //        Reff="/Gallery",
            //        Title="Главная"
            //    }
            //};
            //_navigation = navigation;
                      
            return View(await _hl.GetAllGalleryCategory());
        }

     
        [LayoutFilter]
        [NavigationFilter]
        public async Task<IActionResult> GalleryAll(string id)
        {
            //var model = await _hl.GetGallery(new ObjectId(id));
            var model = await _hl.GetGallery(id);
            var nav = new List<NavigationViewModel>
            {
                
                new NavigationViewModel
                {
                    Id=id,
                    Reff="/GalleryAll/"+id,
                    Title="Галлерея "+ model.Category.Title
                }
            };
            _navigation = nav;
            return View(model);
        }

        [LayoutFilter]
        [NavigationFilter]
        public async Task<IActionResult> AllMaterials(string id)
        {
            
            var model = await _hl.GetAllMaterial(new ObjectId(id));
            AddNavigation("", "/AllMaterials?id=" + id, model.CategoryTitle);
            return View(model);


        }
        [LayoutFilter]
        [NavigationFilter]
        public async Task<IActionResult> SingleMaterial(string id)
        {
            var model = await _hl.GetSingleMaterial(new ObjectId(id));
            AddNavigation("", "/AllMaterials?id=" + model.MaterialCategory.Id, model.MaterialCategory.Title);
            AddNavigation("", "/SingleMaterial?id=" + id, model.Material.Title);
            
            return View("More",model.Material);
        }

       
        [LayoutFilter]
        public async Task<IActionResult> GalleryFull(string id)
        {
            return View(await _hl.GetGallery(new ObjectId(id)));
        }

        [LayoutFilter]
        [NavigationFilter]
        public async Task<IActionResult> ServicesCategory()
        {
            //AddNavigation("", "/", "Главная");
            AddNavigation("", "/ServicesCategory", "Категории услуг");
            
            return View(await _hl.GetServiceCategories());
        }
        [LayoutFilter]
        [NavigationFilter]
        public async Task<IActionResult> Services(string id)
        {

            if (string.IsNullOrEmpty(id))
                return BadRequest();
            var model = await _hl.GetServices(new ObjectId(id));
            if(model!=null)
            {
                string catname = (await repo.ServiceCategoryes.GetOne(x => x.Id == model.First().CategoryId)).Title;
                AddNavigation("", "/ServicesCategory", "Категории услуг");
                AddNavigation("", "/Services/" + id,catname );
            }
            return View(model);
        }

        [LayoutFilter]
        [NavigationFilter]
        public async Task<IActionResult> MoreService(string id)
        {

            var model = await _hl.MoreService(id);
            var service = await repo.Services.GetOne(x => x.Id == new ObjectId(id), cacheName: id);
            var category = await repo.ServiceCategoryes.GetOne(x => x.Id == service.CategoryId, cacheName: id);
            if(service!=null&&category!=null&&model!=null)
            {
                AddNavigation("", "/ServicesCategory", "Категории услуг");
                AddNavigation("", "/Services/" + category.Id, category.Title);
                
            }
            
            return View("More", model);
        }

        [LayoutFilter]
        [NavigationFilter]
        public async Task<IActionResult> About()
        {
            AddNavigation("", "/About/", "О компании");
            return View((await _hl.GetProperty()).About);
        }

          [LayoutFilter]
          [NavigationFilter]
          public async Task<IActionResult> MoreAbout()
        {
            AddNavigation("", "/About/", "О компании");
            return View("More", await _hl.MoreAbout());
        }

        [LayoutFilter]
        [NavigationFilter]
        public async  Task<IActionResult> Contacts()
        {
            AddNavigation("", "/Contacts/", "Контакты");

            return View(await _hl.GetProperty());
        }
        public IActionResult ReadFile(string id)
        {
            var result = _core.ReadFile(_repo, id);
            if (result != null)
                return result;
            return BadRequest();
        }

        void AddNavigation(string id,string reff,string title)
        {
            _navigation.Add(new NavigationViewModel
            {
                Id=id,
                Reff=reff,
                Title=title
            });

        }

    }
}
