﻿using MoonFresh.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoonFresh.ViewModels.Home
{
    public class MaterialSingleViewModel
    {
       public MoreViewModel Material { get; set; }
       public  MaterialCategory MaterialCategory { get; set; }
    }
}
