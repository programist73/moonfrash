﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoonFresh.ViewModels.Home
{
    public class MoreViewModel
    {
        public string Title { get; set; }
        public string Text { get; set; }
    }
}
