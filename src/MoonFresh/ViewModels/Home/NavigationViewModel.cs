﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoonFresh.Core
{
    public class NavigationViewModel
    {
       public string Id { get; set; }
       public  string Reff { get; set; }
       public string Title { get; set; } 
    }
}
