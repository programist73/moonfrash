﻿using MoonFresh.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoonFresh.ViewModels.Home
{
    public class GalleryViewModel
    {
        public GalleryCategory Category { get; set; }
        public IEnumerable<Gallery> Gallerys { get; set; }
    }
}
