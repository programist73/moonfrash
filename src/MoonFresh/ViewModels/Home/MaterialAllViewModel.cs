﻿using MoonFresh.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoonFresh.ViewModels.Home
{
    public class MaterialAllViewModel
    {
        public string CategoryTitle { get; set; }
        public IList<Material> Material { get; set; }
    }
}
