﻿using DbsNetCore.Models.Identity;
using DbsNetCore.MongoDB;
using MoonFresh.Models;

namespace MoonFresh
{
    public  interface IRepository
    {
        Files Files { get; set; }
        MongoHelper<User> Users { get; set; }
        MongoHelper<GalleryCategory> GalleryCategoryes { get; set; }
        MongoHelper<Gallery> Galleres { get; set; }
        MongoHelper<Service> Services { get; set; }
        MongoHelper<ServiceCategory> ServiceCategoryes { get; set; }
        MongoHelper<Material> Materials { get; set; }
        MongoHelper<MaterialCategory> MaterialCategoryes { get; set; }
        MongoHelper<Property> Propertys { get; set; }
    }
}
